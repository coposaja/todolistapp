# TodoListApp
TodoListApp is an application that can list every task you have and haven't done.
Developed with Test Driven Development (TDD).

## Task
Task contains id, name, and status of your task.
<pre>
    Task {
        taskID,
        taskName,
        taskStatus
    }
</pre>
You can check whether your task is correct or not by stringify your task with this code below.
<pre>
    Task task = new Task(1, "Make Task", 1);
    task.toString();
    
    // This will return
    // 1. Make Task [DONE]
</pre>

#### TaskStatus
There is 2 kind of TaskStatus:
<pre>
    DONE : 1,
    NOT DONE : 2
</pre>

## TodoList
TodoList is a class that contain list of Task(s).
It can add more task and also check your list of task by stringify it.
Which will return list of Task(s) separated by new line.

#### Example:
<pre>
    TodoList todoList = new TodoList();
    todoList.addTask(new Task(1, "Make Task", 1));
    todoList.addTask(new Task(2, "Add Task to To Do List", 2));
    todoList.addTask(new Task(3, "Do Task", 1));
    todoList.toString();
    
    // This code will return:
    // 1. Make Task [DONE]\n2. Add Task to To Do List[NOT DONE]\n3. Do Task [DONE]\n
</pre>