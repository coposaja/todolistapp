import com.binus.Task;
import com.binus.TodoList;
import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {
    private TodoList todoList;

    private void generateTodoList() {
        todoList = new TodoList();
        todoList.addTask(new Task(1, "Make Test", 1, "Coding"));
        todoList.addTask(new Task(2, "Do Testing", 2, "Coding"));
        todoList.addTask(new Task(3, "Learn TDD", 2, "Learning"));
    }
    @Test
    public void toStringTodoListNotEmpty() {
        String expected = "1. Make Test - Coding [DONE]\n2. Do Testing - Coding [NOT DONE]\n3. Learn TDD - Learning [NOT DONE]\n";

        generateTodoList();

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void doTaskOnDoneTask() {
        String expected = "1. Make Test - Coding [DONE]";

        generateTodoList();

        assertEquals(expected, todoList.endTask(1).toString());
    }

    @Test
    public void doTaskOnNotDoneTask() {
        String expected = "2. Do Testing - Coding [DONE]";

        generateTodoList();

        assertEquals(expected, todoList.endTask(2).toString());
    }

    @Test
    public void addTaskNullToTodoList() {
        String expected = "1. Make Test - Coding [DONE]\n2. Do Testing - Coding [NOT DONE]\n3. Learn TDD - Learning [NOT DONE]\n";

        generateTodoList();
        todoList.addTask(null);

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void addTaskNotNullToTodoList() {
        String expected = "1. Make Test - Coding [DONE]\n2. Do Testing - Coding [NOT DONE]\n3. Learn TDD - Learning [NOT DONE]\n4. Do Project - Coding [NOT DONE]\n";

        generateTodoList();
        todoList.addTask(new Task(4, "Do Project", 2, "Coding"));

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void deleteTaskEmptyTodoList() {
        String expected = "";

        todoList = new TodoList();
        todoList.deleteTask(1);

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void deleteTaskIdNotFound() {
        String expected = "1. Make Test - Coding [DONE]\n2. Do Testing - Coding [NOT DONE]\n3. Learn TDD - Learning [NOT DONE]\n";

        generateTodoList();
        todoList.deleteTask(100);

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void deleteTaskIdFound() {
        String expected = "2. Do Testing - Coding [NOT DONE]\n3. Learn TDD - Learning [NOT DONE]\n";

        generateTodoList();
        todoList.deleteTask(1);

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void displayByCategoryWithNonExistCategory() {
        String expected = "";

        generateTodoList();

        assertEquals(expected, todoList.displayByCategory("Imaginary"));
    }

    @Test
    public void displayByCategoryWithExistingCategory() {
        String expected = "1. Make Test - Coding [DONE]\n2. Do Testing - Coding [NOT DONE]\n";

        generateTodoList();

        assertEquals(expected, todoList.displayByCategory("Coding"));
    }
}