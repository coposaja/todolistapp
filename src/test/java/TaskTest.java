import com.binus.Task;
import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void toStringTaskDone() {
        String expected = "1. Make Test - Coding [DONE]";
        Task task = new Task(1, "Make Test", 1, "Coding");

        assertEquals(expected, task.toString());
    }

    @Test
    public void toStringTaskNotDone() {
        String expected = "1. Make Test - Coding [NOT DONE]";
        Task task = new Task(1, "Make Test", 2, "Coding");

        assertEquals(expected, task.toString());
    }

    @Test
    public void changeTaskStatus() {
        String expected = "1. Make Test - Coding [DONE]";
        Task task = new Task(1, "Make Test", 2, "Coding");

        assertEquals(expected, task.markAsDone().toString());
    }
}