package com.binus;

import java.util.ArrayList;

public class TodoList {
    private ArrayList<Task> taskList;

    public TodoList() {
        taskList = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Task task : taskList) {
            sb.append(String.format("%s\n", task.toString()));
        }
        return sb.toString();
    }

    private Task getTaskById(int id) {
        for (Task task : this.taskList) {
            if (task.isId(id)) {
                return task;
            }
        }
        return null;
    }

    public Task endTask(int Id) {
        Task toBeDone = getTaskById(Id);

        if(toBeDone == null) {
            return null;
        }
        return toBeDone.markAsDone();
    }

    public void addTask(Task task) {
        if (task != null) {
            taskList.add(task);
        }
    }

    public void deleteTask(int id) {
        Task toBeDeleted = getTaskById(id);
        taskList.remove(toBeDeleted);
    }

    public String displayByCategory(String category) {
        StringBuilder sb = new StringBuilder();
        for (Task task : this.taskList) {
            if(task.isCategory(category)) {
                sb.append(String.format("%s\n", task.toString()));
            }
        }

        return sb.toString();
    }
}
