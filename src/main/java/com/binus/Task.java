package com.binus;

import java.util.HashMap;

public class Task {
    private int id;
    private int status;
    private String name;
    private String category;
    private static HashMap<Integer, String> statusNames = new HashMap<Integer, String>() {{
        put(1, "DONE");
        put(2, "NOT DONE");
    }};

    public Task(int id, String name, int status, String category) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.category = category;
    }

    @Override
    public String toString() {
        return String.format("%d. %s - %s [%s]", id, name, category, statusNames.get(status));
    }

    public Task markAsDone() {
        this.status = 1;
        return this;
    }

    boolean isId(int Id) {
        return this.id == Id;
    }

    boolean isCategory(String category) {
        return this.category.equals(category);
    }
}
