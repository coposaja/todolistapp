package com.binus;

import java.util.Scanner;

class View {
    private Scanner sc = new Scanner(System.in);
    private TodoList todoList = new TodoList();

    View() {
        mainMenu();
    }

    private void mainMenu() {
        while (true) {
            int choice;

            showMenu();
            choice = sc.nextInt();
            sc.nextLine();

            switch (choice) {
                case 0:
                    return;
                case 1:
                    viewAllTask();
                    break;
                case 2:
                    insertTask();
                    break;
                case 3:
                    updateTask();
                    break;
                case 4:
                    deleteTask();
                    break;
                case 5:
                    viewTaskByCategory();
                    break;
                default:
                    break;
            }

            System.out.print("Press enter to proceed...");
            sc.nextLine();
        }
    }

    private void viewTaskByCategory() {
        viewAllTask();

        System.out.print("Insert category: ");
        System.out.println(todoList.displayByCategory(sc.nextLine()));
    }

    private void deleteTask() {
        viewAllTask();
        System.out.print("Choose Task to Delete: ");
        todoList.deleteTask(sc.nextInt());
        sc.nextLine();
        resultNotification("Deleted");
    }

    private void insertTask() {
        int taskId;
        String taskName, taskCategory;

        System.out.print("Insert Task Id: ");
        taskId = sc.nextInt();
        sc.nextLine();

        System.out.print("Insert Task Name: ");
        taskName = sc.nextLine();

        System.out.print("Insert Task Category: ");
        taskCategory = sc.nextLine();

        Task task = new Task(taskId, taskName, 2, taskCategory);
        todoList.addTask(task);
        resultNotification("Inserted");
    }

    private void showMenu() {
        System.out.println("1. View All Task(s)");
        System.out.println("2. Insert Task");
        System.out.println("3. Update Task");
        System.out.println("4. Delete Task");
        System.out.println("5. View Task(s) by Category");
        System.out.println("0. Exit");
        System.out.print("Choose: ");
    }

    private void viewAllTask() {
        System.out.println(todoList.toString());
    }

    private void updateTask() {
        viewAllTask();
        System.out.print("Choose Task to Update: ");
        todoList.endTask(sc.nextInt());
        sc.nextLine();
        resultNotification("Updated");
    }

    private void resultNotification(String action) {
        viewAllTask();
        System.out.println(String.format("Task %s", action));
    }
}
